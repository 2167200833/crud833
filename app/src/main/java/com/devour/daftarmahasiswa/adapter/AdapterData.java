package com.devour.daftarmahasiswa.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devour.daftarmahasiswa.MainActivity;
import com.devour.daftarmahasiswa.R;
import com.devour.daftarmahasiswa.model.DataModel;

import org.w3c.dom.Text;

import java.util.List;

public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData > {
    private List<DataModel> mList;
    private Context ctx;

    public AdapterData(Context ctx, List<DataModel> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist,parent,false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        DataModel dm = mList.get(position);
        holder.nama.setText(dm.getNama());
        holder.nim.setText(dm.getNim());
        holder.prodi.setText(dm.getProdi());
        holder.nohp.setText(dm.getNohp());
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class HolderData extends RecyclerView.ViewHolder{
        TextView nama, nim, prodi, nohp;
        DataModel dm;
        public HolderData(View v)
        {
            super(v);

            nama = (TextView) v.findViewById(R.id.tv_nama);
            nim = (TextView) v.findViewById(R.id.tv_nim);
            prodi = (TextView) v.findViewById(R.id.tv_prodi);
            nohp = (TextView) v.findViewById(R.id.tv_nohp);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, MainActivity.class);
                    goInput.putExtra("id", dm.getId());
                    goInput.putExtra("nama", dm.getNama());
                    goInput.putExtra("nim", dm.getNim());
                    goInput.putExtra("prodi", dm.getProdi());
                    goInput.putExtra("nohp", dm.getNohp());

                    ctx.startActivity(goInput);

                }
            });
        }
    }
}
