-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 04:54 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_mahasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_biodata`
--

CREATE TABLE IF NOT EXISTS `tb_biodata` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `nim` bigint(15) NOT NULL,
  `prodi` varchar(20) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tb_biodata`
--

INSERT INTO `tb_biodata` (`id`, `nama`, `nim`, `prodi`, `nohp`) VALUES
(1, 'Emmanuel Lamanda Pasca Dea', 2147483647, 'Sistem Komputer', '2147483647'),
(2, 'Andi Novianto', 2167200899, 'Sistem Komputer', '81239412'),
(3, 'Gello', 2167200800, 'Sistem Komputer', '08992664818'),
(4, 'Bella', 2167200801, 'Sistem Komputer', '08992664666'),
(5, 'Janjo', 2167200803, 'Sistem Komputer', '0899266777'),
(6, 'Janje', 2167200805, 'Sistem Komputer', '08992666891'),
(7, 'Gilang', 2167200804, 'Sistem Komputer', '08992664462'),
(8, 'Pelif', 2167200808, 'Sistem Komputer', '08995517842');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
