Langkah Penggunaan Input :

1. Isikan data sesuai kolom tersedia
	- Nama (Maksimal 30 Karakter)
	- NIM (Hanya bisa diinput menggunakan angka)
	- Prodi (Maksimal 30 Karakter)
	- No HP (Hanya bisa diinput menggunakan angka)
2. Tekan tombol Save
(Gambar 1.jpg)

Untuk melihat data tekan Tombol LIHAT DATA
(Gambar 2.jpg)

Untuk mengedit data tekan data yang ingin di edit :
(Gambar 3.jpg)
1. Lakukan edit data sesuai dengan kolom tersedia
2. Tekan tombol Update jika sudah selesai
(Gambar 4.jpg)

Untuk Menghapus data tekan Tombol HAPUS DATA
(Gambar 5.jpg)


****************************************************
****************************************************
KEKURANGAN PADA APLIKASI INI ADALAH KETIKA UPDATE
/ HAPUS DATA, MASIH TIDAK OTOMATIS TERUPDATE DATANYA

SEHINGGA MASIH DIPERLUKAN KEMBALI KE HALAMAN UTAMA
LALU MENEKAN TOMBOL LIHAT DATA UNTUK DATA TERBARU
****************************************************
****************************************************

FILE CRUD PHP ADA DI FOLDER WEBSITE